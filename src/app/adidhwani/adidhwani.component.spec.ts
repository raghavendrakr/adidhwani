import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdidhwaniComponent } from './adidhwani.component';

describe('AdidhwaniComponent', () => {
  let component: AdidhwaniComponent;
  let fixture: ComponentFixture<AdidhwaniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdidhwaniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdidhwaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
