import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuladhwaniComponent } from './muladhwani.component';

describe('MuladhwaniComponent', () => {
  let component: MuladhwaniComponent;
  let fixture: ComponentFixture<MuladhwaniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MuladhwaniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MuladhwaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
