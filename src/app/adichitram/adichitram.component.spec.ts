import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdichitramComponent } from './adichitram.component';

describe('AdichitramComponent', () => {
  let component: AdichitramComponent;
  let fixture: ComponentFixture<AdichitramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdichitramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdichitramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
