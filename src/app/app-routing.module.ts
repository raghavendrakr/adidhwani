import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { AdichitramComponent } from './adichitram/adichitram.component';
import { MuladhwaniComponent } from './muladhwani/muladhwani.component';
import { AdidhwaniComponent } from './adidhwani/adidhwani.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'about', component: AboutComponent},
  { path: 'adidhwani', component: AdidhwaniComponent},
  { path: 'muladhwani', component: MuladhwaniComponent},
  { path: 'adichitram', component: AdichitramComponent},
  { path: 'contact', component: ContactComponent}, 
  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
